# javakatarislemleri

Oluşturulacak veriler.txt isimli dosya içerisine kontrol edilecek karakter katarları satır satır eklenecektir. İlk satırda kabul edilecek ve işlenebilecek katarlar, ikinci satırda da kabul edilmeyecek katarlar eklenecektir. (tüm karakterler küçük harflerden oluşacaktır.)

* Bir metodun içerisinde dosyadaki satırların değerleri okunacaktır.
* Bir metot ile karakterlerin kurala uygun olup olmadığı kontrol edilecektir. Geriye doğru yanlış değeri döndürecektir.
* Eğer kurala uygun değilse “karakter katarı uygun değildir” mesajı verecektir.
* Eğer karakter kurala uygun ise işlenecek verilerin ayrıştırılması için ayrı metoda gönderilecektir. Geriye işlenecek değerler bir liste içerisinde döndürülecektir.
* Eğer listede eleman varsa işlenmek amacıyla, işleyecek metoda gönderilecek ve sonuç geriye döndürülecektir.

Sonuç ekrana yazdırılacaktır.

Katarlar fonk2t kelimesi ile başlayacak ve devamında 2’nin katı olacak şekilde rakamlar girilecek son karakterler “f1” veya “f2” olarak girilecektir. Son karakterler eğer “f1” ise aşağıdaki 1. fonksiyon “f2” ise aşağıdaki 2. Fonksiyona göre hesaplama yapılacaktır.
(örnek: fonk2t356417f1)

 x: rakam
 i: rakam sıra numarası